from random import choice
from unicodedata import normalize, category

domains = ["@gmail.com", "@outlook.com", "@hotmail.com", "@yandex.ru"]

with open("nombres.txt", "r") as namesFile:
    names = namesFile.read().splitlines()

with open("apellidos.txt", "r") as lastNamesFile:
    lastNames = lastNamesFile.read().splitlines()


def removeAccents(text):
    return ''.join(c for c in normalize('NFD', text)
                   if category(c) != 'Mn')


names = [removeAccents(text).lower() for text in names]
lastNames = [removeAccents(text).lower() for text in lastNames]

emails = []
for i in range(100000):
    name = choice(names)
    lastName = choice(lastNames)
    domain = choice(domains)
    email = f"{name}_{lastName}{domain}"
    emails.append(email)

with open("../producer/emails.txt", "w") as emailsFile:
    emailsFile.write("\n".join(emails))