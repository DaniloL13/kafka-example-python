bootstrapServers = ["127.0.0.1:9092", "127.0.0.1:9093", "127.0.0.1:9094"]
kafkaTopic = "test-topic"
kafkaGroup = "test-group"
kafkaClientId = "test-client"
kafkaMaxRecords = 10
