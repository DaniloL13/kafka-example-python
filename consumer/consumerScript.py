from json import loads
from time import sleep

from kafka import KafkaConsumer

from consumer.configuration import kafkaTopic, bootstrapServers, kafkaGroup, kafkaClientId, kafkaMaxRecords


def doSomething(data):
    for item in data:
        print(item.value)


def createCondumer():
    print("Creating kafka consumer")
    consumer = KafkaConsumer(kafkaTopic, bootstrap_servers=bootstrapServers,
                             group_id=kafkaGroup,
                             client_id=kafkaClientId,
                             enable_auto_commit=True,
                             auto_offset_reset='latest',
                             value_deserializer=lambda v: loads(v.decode('utf-8')),
                             max_poll_interval_ms=1200000)
    return consumer


def startConsumer():
    print("Starting kafka consumer")
    consumer = createCondumer()
    while True:
        print("Retrieving data")
        items = consumer.poll(max_records=int(kafkaMaxRecords)).values()
        listItems = list(items)
        if len(listItems) > 0:
            for item in listItems:
                print(f"Se recuperaron [{len(item)}] items a procesar")
                doSomething(item)
                # no pull data until finish current data
        sleep(1)


startConsumer()