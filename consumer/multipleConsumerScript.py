from json import loads
from multiprocessing import Process
from time import sleep

from kafka import KafkaConsumer

from consumer.configuration import kafkaTopic, bootstrapServers, kafkaGroup, kafkaClientId, kafkaMaxRecords


def doSomething(data, index):
    consumerName = f"consumer-{index}"
    for item in data:
        print(f"Item [{item.value}] retrieved with consumer [{consumerName}]")


def createConsumer(value):
    consumerName = f"consumer-{value}"
    print(f"Creating kafka consumer [{consumerName}]")
    consumer = KafkaConsumer(kafkaTopic, bootstrap_servers=bootstrapServers,
                             group_id=kafkaGroup,
                             client_id=f"{kafkaClientId}-{value}",
                             enable_auto_commit=True,
                             auto_offset_reset='latest',
                             value_deserializer=lambda v: loads(v.decode('utf-8')),
                             max_poll_interval_ms=1200000)
    return consumer


def consumerWorker(consumer, index):
    consumerName = f"consumer-{index}"
    while True:
        print(f"Retrieving data from consumer [{consumerName}]")
        items = consumer.poll(max_records=int(kafkaMaxRecords)).values()
        listItems = list(items)
        if len(listItems) > 0:
            for item in listItems:
                print(f"Total [{len(item)}] items retrieved from consumer [{consumerName}]")
                doSomething(item, index)
                # no pull data until finish current data
        sleep(1)


def startConsumers():
    print("Starting kafka consumers")
    MAX_WORKERS = 5
    processes = [Process(target=consumerWorker, args=(createConsumer(i), i)) for i in range(MAX_WORKERS)]
    for process in processes:
        process.start()


startConsumers()
