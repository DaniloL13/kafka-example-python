from random import choice

from subjectEnum import Subject

with open("./emails.txt", "r") as f:
    data = f.read().splitlines()



def parseEmail(email):
    item = {
        "email": email,
    }
    subject = choice(list(Subject))
    item["subject"] = subject.value
    email = email.split("@")[0]
    name, lastName = email.split("_")
    text = f"Dear {name.capitalize()} {lastName.capitalize()}\n\n We want to inform you about your process:\n\n {subject.value} \n\n Te task has finished. \n\n Best regards."
    item["text"] = text
    return item


items = [parseEmail(d) for d in data if d]
