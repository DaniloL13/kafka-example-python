from enum import Enum


class Subject(Enum):
    DOWNLOAD_COMPLETED = "Download completed"
    DOWNLOAD_FAILED = "Download failed"
    SUGGESTION_RECEIVED = "Suggestion received"
    BATCH_COMPLETED = "Batch processing completed"
    BATCH_FAILED = "Batch processing failed"
