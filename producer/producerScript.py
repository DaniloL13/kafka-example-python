import random
from _socket import gethostname
from hashlib import md5
from json import dumps
from time import sleep

from items import items

from confluent_kafka import Producer

from producer.configuration import bootstrapServers, kafkaTopic


def callback(err, event):
    if err:
        print(f'Produce to topic {event.topic()} failed for event: {event.key()}')
    else:
        val = event.value().decode('utf8')
        print(f'The item [{val}] with key [{event.key()}] was sent to [{event.topic()}] at partition [{event.partition()}]')


def sendMessage(kafkaProducer, key, item):
    kafkaProducer.produce(kafkaTopic, key=key, value=dumps(item).encode("utf-8"), on_delivery=callback)


def getProducer():
    config = {'bootstrap.servers': ",".join(bootstrapServers),
              'client.id': gethostname()
              }
    producer = Producer(config)
    return producer


def startProducer(limit=100):
    print("Starting kafka producer")
    kafkaProducer = getProducer()
    temp = items.copy()
    while True:
        print("Sending items to kafka")
        temp = random.choices(temp, k=limit) if limit else temp
        for item in temp:
            key = md5(item.get("text").encode("utf-8")).hexdigest()
            sendMessage(kafkaProducer, key, item)
        kafkaProducer.flush()
        sleep(10)


limit = None
startProducer(limit)
